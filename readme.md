#Fizz Buzz

Simple jar that can iterates over command line args and prints
- the number OR
- 'fizz' for numbers that are multiples of 3 OR
- 'buzz' for numbers that are multiples of 5 OR
- 'fizzbuzz' for numbers that are multiples of 15
- 'lucky' for numbers containing a 3. This overrides any of the above rules
- A report of each item type that has been processed
  
e.g. if I run the program over a range from 1-20 I should get the following output
~~~~
1 2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14 fizzbuzz 16 17 fizz 19 buzz
fizz: 4
buzz: 3
fizzBuzz: 1
lucky: 2
integer: 10
~~~~

##Prerequisites
- Access to http://mvnrepository.com/
- Java 8
- Command line terminal

##How to build test and run

On Mac:
~~~~
# Open terminal and navigate to root of project
./gradlew clean build fatJar
java -jar build/libs/fizz-buzz-fat-1.0.jar 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
~~~~

On Windows:
~~~~
# This should work but I don't have a windows machine to test it
# Open command line and navigate to root of project
gradlew.bat clean build fatJar
java -jar build\libs\fizz-buzz-fat-1.0.jar 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
~~~~

##Notes to accessor
- My .git folder is available so you can review my all be it short commit history
- Adding a logging framework like slf/logback felt beyond the scope of the exercise, I did not use System out for logging to not confuse the program output
- Adding a DI framework like the ones from Spring / Google felt beyond the scope of the exercise
- I was tempted to pull the constants in FizzBuzzServiceImpl to an enum but I concluded this would not add anything, happy to be challenged in a code review on this though
- I was tempted to pull the report generation logic to a new service but I concluded this would not add anything, happy to be challenged in a code review on this though
- The is the point at which I would be happy to raise a PR get review comments from my fellow dev