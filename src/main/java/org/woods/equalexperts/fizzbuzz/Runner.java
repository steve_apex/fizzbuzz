package org.woods.equalexperts.fizzbuzz;

public class Runner {


    /**
     * Main entry point into the code
     *
     * @param args
     */
    public static void main(String[] args) {
        FizzBuzzService fizzBuzzService = new FizzBuzzServiceImpl();

        try {
            String fizzBuzz = fizzBuzzService.prepFizzBuzz(args);

            System.out.println(fizzBuzz);
            FizzBuzzReport report = fizzBuzzService.generateReport(fizzBuzz);
            System.out.println(report.print());


        } catch (Exception e) {
            System.err.println("Failed to process user input");
            e.printStackTrace();
        }
    }
}
