package org.woods.equalexperts.fizzbuzz;

public class FizzBuzzReport {

    private long fizz;
    private long buzz;
    private long fizzBuzz;
    private long lucky;
    private long integer;

    public long getFizz() {
        return fizz;
    }

    public long getBuzz() {
        return buzz;
    }

    public long getFizzBuzz() {
        return fizzBuzz;
    }

    public long getLucky() {
        return lucky;
    }

    public long getInteger() {
        return integer;
    }

    public void setFizz(long fizz) {
        this.fizz = fizz;
    }

    public void setBuzz(long buzz) {
        this.buzz = buzz;
    }

    public void setFizzBuzz(long fizzBuzz) {
        this.fizzBuzz = fizzBuzz;
    }

    public void setLucky(long lucky) {
        this.lucky = lucky;
    }

    public void setInteger(long integer) {
        this.integer = integer;
    }

    public String print() {
        return  "fizz: " + fizz
                + "\nbuzz: " + buzz
                + "\nfizzBuzz: " + fizzBuzz
                + "\nlucky: " + lucky
                + "\ninteger: " + integer;
    }
}
