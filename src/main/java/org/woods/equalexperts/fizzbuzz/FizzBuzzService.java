package org.woods.equalexperts.fizzbuzz;

import java.util.List;

public interface FizzBuzzService {

    /**
     * Simple entry points intended to be called
     * by a command line runner which will convert
     * the String[] input to a List<Integer> and
     * then calls the overloaded method
     *
     * @param inputs
     * @return
     */
    String prepFizzBuzz(String[] inputs);


    /**
     * Preps the fizz buzz string ready to be
     * printed by the runner according to the
     * rules found in the read me.
     *
     * Assumes the inputs are not null
     *
     * @param inputs used to create fizz buzz string
     * @return fizz buzz string ready to print
     */
    String prepFizzBuzz(List<Integer> inputs);

    /**
     * Preps a count of each type
     * (fizz/buzz/fizzbuzz/lucky/integer)
     * of value found in the fizzBuzzOutput
     * @param fizzBuzzOutput
     * @return
     */
    FizzBuzzReport generateReport(String fizzBuzzOutput);

}
