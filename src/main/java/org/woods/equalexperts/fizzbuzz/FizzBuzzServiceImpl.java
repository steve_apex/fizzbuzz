package org.woods.equalexperts.fizzbuzz;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import static com.google.common.base.CharMatcher.whitespace;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Splitter.on;
import static com.google.common.primitives.Ints.tryParse;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class FizzBuzzServiceImpl implements FizzBuzzService {


    protected static final String MULTIPLE_OF_THREE_INDICATOR = "fizz";
    protected static final String MULTIPLE_OF_FIVE_INDICATOR = "buzz";
    protected static final String CONTAINS_3_INDICATOR = "lucky";
    protected static final String INTEGER_INDICATOR = "integer";

    @Override
    public String prepFizzBuzz(String[] inputs) {

        checkArgument(nonNull(inputs), "No fizz buzz input provided");

        final int numOfInputs = inputs.length;

        final List<Integer> numbers = new ArrayList<>(numOfInputs);
        Stream.of(inputs).forEach(s -> add(numbers, s));

        return prepFizzBuzz(numbers);
    }

    @Override
    public String prepFizzBuzz(List<Integer> inputs) {
        StringBuilder stringBuilder = new StringBuilder();

        for (Integer input: inputs) {

            if(contains(input, "3")) {
                stringBuilder.append(CONTAINS_3_INDICATOR);
            } else {
                handelFizzBuzz(stringBuilder, input);
            }

            stringBuilder.append(" ");
        }

        return whitespace().trimFrom(stringBuilder.toString());
    }

    /**
     * Splits the string on space and counts occurrences of
     * fizz, buzz, fizzBuzz, lucky and integer buckets.
     *
     * If output is encountered that does not fit into
     * one of the above bucket it is ignored
     *
     * @param fizzBuzzOutput
     * @return
     */
    @Override
    public FizzBuzzReport generateReport(String fizzBuzzOutput) {
        checkArgument(nonNull(fizzBuzzOutput), "No fizz buzz output provided");

        List<String> strings = on(' ').splitToList(fizzBuzzOutput);

        // convert any string integer in the list to string INTEGER_INDICATOR
        strings = strings.stream().map(s -> nonNull(tryParse(s)) ? INTEGER_INDICATOR : s).collect(toList());

        // aggregate the list to map with key being the value e.g. fizz and value being the number of occurrences
        Map<String, Long> aggregation =
                strings.stream().collect(groupingBy(Function.identity(), counting()));

        return generateReport(aggregation);
    }

    /**
     * Simple helper method to generate the report
     * @param aggregation
     * @return
     */
    private FizzBuzzReport generateReport(Map<String, Long> aggregation) {
        FizzBuzzReport fizzBuzzReport = new FizzBuzzReport();
        fizzBuzzReport.setBuzz(removeOrDefault(MULTIPLE_OF_FIVE_INDICATOR, aggregation));

        fizzBuzzReport.setFizz(removeOrDefault(MULTIPLE_OF_THREE_INDICATOR, aggregation));

        fizzBuzzReport.setFizzBuzz(removeOrDefault(MULTIPLE_OF_THREE_INDICATOR  + MULTIPLE_OF_FIVE_INDICATOR, aggregation));

        fizzBuzzReport.setLucky(removeOrDefault(CONTAINS_3_INDICATOR, aggregation));

        fizzBuzzReport.setInteger(removeOrDefault(INTEGER_INDICATOR, aggregation));

        if(!aggregation.isEmpty()) {
            System.err.println("[WARN] unexpected element(s) found in list will be ignored");
        }

        return fizzBuzzReport;
    }

    /**
     * Returns 0 if key not available in the map
     *
     * @param key
     * @param map
     * @return
     */
    private Long removeOrDefault(String key, Map<String, Long> map) {
        return map.containsKey(key) ? map.remove(key) : 0L;
    }

    /**
     * Appends MULTIPLE_OF_THREE_INDICATOR when
     * input is multiple of 3
     *
     * Appends MULTIPLE_OF_FIVE_INDICATOR when
     * input is multiple of 5
     *
     * Appends the number if not a multiple of
     * 3 or 5
     *
     * @param stringBuilder
     * @param input
     */
    private void handelFizzBuzz(StringBuilder stringBuilder, Integer input) {
        final boolean multipleOf3 = isMultipleOf(3, input);
        final boolean multipleOf5 = isMultipleOf(5, input);

        if (multipleOf3) {
            stringBuilder.append(MULTIPLE_OF_THREE_INDICATOR);
        }

        if (multipleOf5) {
            stringBuilder.append(MULTIPLE_OF_FIVE_INDICATOR);
        }

        if (!multipleOf3 && !multipleOf5) {
            stringBuilder.append(input);
        }
    }

    /**
     * Converts the input to a string and tests
     * to see if it contains the string
     *
     * input = 123 searchFor = 3 result = true
     * input = 12 searchFor = 3 result = false
     *
     * @param input to search in
     * @param searchFor
     * @return
     */
    private boolean contains(Integer input, String searchFor) {
        return String.valueOf(input).contains(searchFor);
    }

    /**
     *
     * mod = 3 number = 3 result = true
     * mod = 3 number = 5 result = false
     *
     * @param mod
     * @param number
     * @return true if multiple
     */
    private boolean isMultipleOf(int mod, Integer number) {
        return number % mod == 0;
    }

    /**
     * Converts the String to an Integer
     *
     * If the conversion passes it is added
     * to the list if it fails an exception
     * is propagated
     *
     * @param numbers to add to
     * @param input to convert
     */
    private void add(List<Integer> numbers, String input) {

        final Integer integer = tryParse(input);
        checkArgument(nonNull(integer), "Invalid input encountered '%s' only integers are allowed", input);
        numbers.add(integer);

    }
}
