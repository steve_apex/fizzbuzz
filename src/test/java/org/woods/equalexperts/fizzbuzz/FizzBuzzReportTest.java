package org.woods.equalexperts.fizzbuzz;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class FizzBuzzReportTest {

    private static final String EXPECTED_REPORT = "fizz: 1\nbuzz: 2"
            + "\nfizzBuzz: 3\nlucky: 4\ninteger: 5";

    @Test
    public void testFizzBuzzReportIsPrintedAsExpected() {
        // setup
        FizzBuzzReport report = new FizzBuzzReport();

        report.setFizz(1);
        report.setBuzz(2);
        report.setFizzBuzz(3);
        report.setLucky(4);
        report.setInteger(5);

        // execute
        String result = report.print();

        // verify
        assertThat(result, is(EXPECTED_REPORT));

    }
}
