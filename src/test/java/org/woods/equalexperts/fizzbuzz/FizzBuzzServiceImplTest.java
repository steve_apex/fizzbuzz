package org.woods.equalexperts.fizzbuzz;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.rules.ExpectedException.none;
import static org.woods.equalexperts.fizzbuzz.FizzBuzzServiceImpl.CONTAINS_3_INDICATOR;
import static org.woods.equalexperts.fizzbuzz.FizzBuzzServiceImpl.MULTIPLE_OF_FIVE_INDICATOR;
import static org.woods.equalexperts.fizzbuzz.FizzBuzzServiceImpl.MULTIPLE_OF_THREE_INDICATOR;

@RunWith(BlockJUnit4ClassRunner.class)
public class FizzBuzzServiceImplTest {

    private static final String EXPECTED_SPEC_RESULT = "1 2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14 fizzbuzz 16 17 fizz 19 buzz";

    private FizzBuzzServiceImpl fizzBuzzService;

    @Rule
    public ExpectedException expectedException = none();

    @Before
    public void setup() {
        fizzBuzzService = new FizzBuzzServiceImpl();
    }

    @Test
    public void whenInputIsEmptyExpectEmptyString() {
        // setup
        final String[] input = {};

        // execute
        final String result = fizzBuzzService.prepFizzBuzz(input);

        // verify
        assertThat(result, is(""));
    }

    @Test
    public void whenInputIsNullExpectError() {
        // setup
        final String[] input = null;

        // verify
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("No fizz buzz input provided");

        // execute
        fizzBuzzService.prepFizzBuzz(input);

    }

    @Test
    public void whenInputIsNotAnIntegerExpectError() {
        // setup
        final String[] input = {"5.5"};

        // verify
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Invalid input encountered '5.5' only integers are allowed");

        // execute
        fizzBuzzService.prepFizzBuzz(input);

    }

    @Test
    public void whenInputIsMultipleOf3ExpectIndicator() {
        // setup
        final List<Integer> input = newArrayList(9);

        // execute
        final String result = fizzBuzzService.prepFizzBuzz(input);

        // verify
        assertThat(result, is(MULTIPLE_OF_THREE_INDICATOR));

    }

    @Test
    public void whenInputIsMultipleOf5ExpectIndicator() {
        // setup
        final List<Integer> input = newArrayList(20);

        // execute
        final String result = fizzBuzzService.prepFizzBuzz(input);

        // verify
        assertThat(result, is(MULTIPLE_OF_FIVE_INDICATOR));

    }

    @Test
    public void whenInputIsMultipleOf15ExpectIndicator() {
        // setup
        final List<Integer> input = newArrayList(45);

        // execute
        final String result = fizzBuzzService.prepFizzBuzz(input);

        // verify
        assertThat(result, is(MULTIPLE_OF_THREE_INDICATOR + MULTIPLE_OF_FIVE_INDICATOR));

    }

    @Test
    public void whenInputContains3ExpectLucky() {
        // setup
        final List<Integer> input = newArrayList(30);

        // execute
        final String result = fizzBuzzService.prepFizzBuzz(input);

        // verify
        assertThat(result, is(CONTAINS_3_INDICATOR));

    }

    @Test
    public void whenInputIsNotMultipleOf3or5ExpectNumberBack() {
        // setup
        final List<Integer> input = newArrayList(2);

        // execute
        final String result = fizzBuzzService.prepFizzBuzz(input);

        // verify
        assertThat(result, is("2"));

    }

    @Test
    public void whenInputIsListFrom1to20InputIsSameAsSpecValues() {
        // setup
        final List<Integer> input = newArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
                                                 12, 13, 14, 15, 16, 17, 18, 19, 20);

        // execute
        final String result = fizzBuzzService.prepFizzBuzz(input);

        // verify
        assertThat(result, is(EXPECTED_SPEC_RESULT));

    }

    @Test
    public void whenGeneratingReportWithoutOutputExpectError() {
        // setup
        String fizzBuzzOutput = null;

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("No fizz buzz output provided");

        // execute
        fizzBuzzService.generateReport(fizzBuzzOutput);

    }

    @Test
    public void whenGeneratingReportFrom1to20InputIsSameAsSpecValues() {
        // setup
        final List<Integer> input = newArrayList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
                12, 13, 14, 15, 16, 17, 18, 19, 20);
        final String fizzBuzzOutput = fizzBuzzService.prepFizzBuzz(input);


        // execute
        FizzBuzzReport report = fizzBuzzService.generateReport(fizzBuzzOutput);

        // verify
        assertThat(report.getFizz(), is(4L));
        assertThat(report.getBuzz(), is(3L));
        assertThat(report.getFizzBuzz(), is(1L));
        assertThat(report.getLucky(), is(2L));
        assertThat(report.getInteger(), is(10L));

    }

    @Test
    public void whenGeneratingReportFromWithUnexpectedInputTheInputIsIgnored() {
        // setup
        final String fizzBuzzOutput = "fizz fizz buzz invalid";

        // execute
        FizzBuzzReport report = fizzBuzzService.generateReport(fizzBuzzOutput);

        // verify
        assertThat(report.getFizz(), is(2L));
        assertThat(report.getBuzz(), is(1L));
        assertThat(report.getFizzBuzz(), is(0L));
        assertThat(report.getLucky(), is(0L));
        assertThat(report.getInteger(), is(0L));

    }

}