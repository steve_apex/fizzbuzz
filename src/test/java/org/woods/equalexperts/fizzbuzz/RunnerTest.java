package org.woods.equalexperts.fizzbuzz;

import org.junit.Test;

public class RunnerTest {

    @Test
    public void testWhenUserInputIsValidItCanBeProcessedWithoutError() {
        // setup
        String[] args = {"3"};

        // execute
        Runner.main(args);

        // verify no exception is thrown
    }

    @Test
    public void testWhenErrorOccursItIsLoggedAndSunk() {
        // setup
        String[] args = {"bad"};

        // execute
        Runner.main(args);

        // verify no exception is thrown
    }
}
